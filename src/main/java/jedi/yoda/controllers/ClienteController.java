package jedi.yoda.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jedi.yoda.models.ClienteModel;
import jedi.yoda.service.ClienteService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/cliente")
public class ClienteController {
    
    @Autowired
    ClienteService clienteService;

    @GetMapping("/obtener")//Funciona
    public ArrayList<ClienteModel> obtenerCliente(){
        return clienteService.obtenerCliente();
    }
    

    @PostMapping("/guardar")//Funciona
    public ClienteModel guardarCliente(@RequestBody ClienteModel cliente){
        return clienteService.guardarCliente(cliente);
    }

    @DeleteMapping("/eliminar/{idCliente}")//Funciona
    public String eliminarCliente(@PathVariable("idCliente") Integer idCliente){
        boolean ok = this.clienteService.eliminarCliente(idCliente);
        if(ok){
            return "Cliente eliminado";
        } else {
            return "Cliente no eliminado";
        }
    }

    @PutMapping("/actualizar")//Funciona
    public void actualizarUsuario(@RequestBody ClienteModel cliente){
        clienteService.actualizarCliente(cliente);
    }

    @GetMapping("/{cedula}")//Funciona
    public ArrayList<ClienteModel> obtenerPorCedula(@PathVariable("cedula") Integer cedula){
        
        return this.clienteService.obtenerPorCedula(cedula);
    }

    @DeleteMapping("/eliminar/{cedula}")//No funciona
    public String eliminarCedula(@PathVariable("cedula") Integer cedula){
        boolean ok = clienteService.eliminarPorCedula(cedula);
        if(ok){
            return "Cliente eliminado";
        } else {
            return "Cliente no eliminado";
        }
    }

    @GetMapping("/obtenerId/{idCliente}")//Funciona
    public Optional<ClienteModel> buscarPorId(@PathVariable("idCliente") Integer idCliente){
        return this.clienteService.obtenerPorId(idCliente);
    }
    
}