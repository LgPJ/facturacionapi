package jedi.yoda.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jedi.yoda.models.ProductoModel;
import jedi.yoda.service.ProductoService;

@RestController
@RequestMapping("/producto")
public class ProductoController {
     
    @Autowired
    ProductoService productoService;

    @GetMapping("/obtener")
    public ArrayList<ProductoModel> obtenerProducto(){
        return productoService.obtenerProducto();
    }

    @PostMapping("/guardar")
    public ProductoModel guardarProducto(@RequestBody ProductoModel producto){
        return productoService.guardarProducto(producto);
    }
    
    @DeleteMapping("/eliminar/{idProducto}")
    public String eliminarProducto(@PathVariable("idProducto") Long idProducto){
        boolean ok = this.productoService.eliminarProducto(idProducto);
        if(ok){
            return "Producto eliminado";
        } else {
            return "Producto no eliminado";
        }
    }

    



}
