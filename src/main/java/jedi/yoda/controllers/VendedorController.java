package jedi.yoda.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jedi.yoda.models.VendedorModel;
import jedi.yoda.service.VendedorService;

@RestController
@RequestMapping("/vendedor")
public class VendedorController {
    
    @Autowired
    VendedorService vendedorService;

    @GetMapping("/obtener")
    public ArrayList<VendedorModel> obtenerVendedor(){
        return vendedorService.obtenerVendedor();
    }

    @PostMapping("/guardar")
    public VendedorModel guardarCliente(@RequestBody VendedorModel vendedor){
        return vendedorService.guardarVendedor(vendedor);
    }

    @PutMapping("/actualizar")//Funciona
    public void actualizarVendedor(@RequestBody VendedorModel vendedor){
        vendedorService.actualizarCliente(vendedor);
    }

    @DeleteMapping("/eliminar/{idVendedor}")
    public String eliminaVendedor(@PathVariable("idVendedor") Integer idVendedor){
        boolean ok = this.vendedorService.eliminarVendedor(idVendedor);
        if(ok){
            return "Vendedor eliminado";
        } else {
            return "Vendedor no eliminado";
        }
    }

    @GetMapping("/obtenerId/{idVendedor}")
    public Optional<VendedorModel> buscarPorId(@PathVariable("idVendedor") Integer idVendedor){
        return this.vendedorService.obtenerPorId(idVendedor);
    }
}
