package jedi.yoda.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.yoda.models.ProductoModel;

@Repository
public interface ProductoRepository extends CrudRepository<ProductoModel, Long> {
    
    
}
