package jedi.yoda.repository;



import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.yoda.models.ClienteModel;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteModel, Integer>{
     
    ArrayList<ClienteModel> findByCedula(Integer cedula);

    void deleteByCedula(Integer cedula);

    
}
