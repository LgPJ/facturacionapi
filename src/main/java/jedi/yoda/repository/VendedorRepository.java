package jedi.yoda.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.yoda.models.VendedorModel;

@Repository
public interface VendedorRepository extends CrudRepository<VendedorModel, Integer> {

}
