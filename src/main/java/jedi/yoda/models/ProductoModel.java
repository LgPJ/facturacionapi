package jedi.yoda.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class ProductoModel implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer idProducto;

    private String nombreProducto;
    private Integer codigoProducto;
    private Float precioProducto; 
    private Float cantidadProducto;
    private String descripcionProducto;


    public ProductoModel() {
    }


    public ProductoModel(Integer idProducto, String nombreProducto, Integer codigoProducto, Float precioProducto, Float cantidadProducto, String descripcionProducto) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.codigoProducto = codigoProducto;
        this.precioProducto = precioProducto;
        this.cantidadProducto = cantidadProducto;
        this.descripcionProducto = descripcionProducto;
    }

    public Integer getIdProducto() {
        return this.idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return this.nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Integer getCodigoProducto() {
        return this.codigoProducto;
    }

    public void setCodigoProducto(Integer codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public Float getPrecioProducto() {
        return this.precioProducto;
    }

    public void setPrecioProducto(Float precioProducto) {
        this.precioProducto = precioProducto;
    }

    public Float getCantidadProducto() {
        return this.cantidadProducto;
    }

    public void setCantidadProducto(Float cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public String getDescripcionProducto() {
        return this.descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public ProductoModel idProducto(Integer idProducto) {
        setIdProducto(idProducto);
        return this;
    }

    public ProductoModel nombreProducto(String nombreProducto) {
        setNombreProducto(nombreProducto);
        return this;
    }

    public ProductoModel codigoProducto(Integer codigoProducto) {
        setCodigoProducto(codigoProducto);
        return this;
    }

    public ProductoModel precioProducto(Float precioProducto) {
        setPrecioProducto(precioProducto);
        return this;
    }

    public ProductoModel cantidadProducto(Float cantidadProducto) {
        setCantidadProducto(cantidadProducto);
        return this;
    }

    public ProductoModel descripcionProducto(String descripcionProducto) {
        setDescripcionProducto(descripcionProducto);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ProductoModel)) {
            return false;
        }
        ProductoModel productoModel = (ProductoModel) o;
        return Objects.equals(idProducto, productoModel.idProducto) && Objects.equals(nombreProducto, productoModel.nombreProducto) && Objects.equals(codigoProducto, productoModel.codigoProducto) && Objects.equals(precioProducto, productoModel.precioProducto) && Objects.equals(cantidadProducto, productoModel.cantidadProducto) && Objects.equals(descripcionProducto, productoModel.descripcionProducto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProducto, nombreProducto, codigoProducto, precioProducto, cantidadProducto, descripcionProducto);
    }

    @Override
    public String toString() {
        return "{" +
            " idProducto='" + getIdProducto() + "'" +
            ", nombreProducto='" + getNombreProducto() + "'" +
            ", codigoProducto='" + getCodigoProducto() + "'" +
            ", precioProducto='" + getPrecioProducto() + "'" +
            ", cantidadProducto='" + getCantidadProducto() + "'" +
            ", descripcionProducto='" + getDescripcionProducto() + "'" +
            "}";
    }
    
}
