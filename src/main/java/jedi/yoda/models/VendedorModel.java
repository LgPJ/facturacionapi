package jedi.yoda.models;


import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vendedor")
public class VendedorModel implements Serializable {
    
    @Id
    @Column(unique = true, nullable = false)
    private Integer idVendedor;
    
    private String nombre;
    private String apellido;
    private Integer cedula;
 


    public VendedorModel() {
    }

    public VendedorModel(Integer idVendedor, String nombre, String apellido, Integer cedula) {
        this.idVendedor = idVendedor;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    public Integer getIdVendedor() {
        return this.idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getCedula() {
        return this.cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public VendedorModel idVendedor(Integer idVendedor) {
        setIdVendedor(idVendedor);
        return this;
    }

    public VendedorModel nombre(String nombre) {
        setNombre(nombre);
        return this;
    }

    public VendedorModel apellido(String apellido) {
        setApellido(apellido);
        return this;
    }

    public VendedorModel cedula(Integer cedula) {
        setCedula(cedula);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof VendedorModel)) {
            return false;
        }
        VendedorModel vendedorModel = (VendedorModel) o;
        return Objects.equals(idVendedor, vendedorModel.idVendedor) && Objects.equals(nombre, vendedorModel.nombre) && Objects.equals(apellido, vendedorModel.apellido) && Objects.equals(cedula, vendedorModel.cedula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVendedor, nombre, apellido, cedula);
    }

    @Override
    public String toString() {
        return "{" +
            " idVendedor='" + getIdVendedor() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", apellido='" + getApellido() + "'" +
            ", cedula='" + getCedula() + "'" +
            "}";
    }
    
}
