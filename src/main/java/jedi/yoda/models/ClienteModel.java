package jedi.yoda.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;


@Entity
@Table(name = "cliente")
public class ClienteModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer idCliente;

    private Integer codigoVendedor;
    private String nombre;
    private String apellido;
    private String direccion;
    private Integer cedula;

    public ClienteModel() {
    }

    public ClienteModel(Integer idCliente, Integer codigoVendedor, String nombre, String apellido, String direccion, Integer cedula) {
        this.idCliente = idCliente;
        this.codigoVendedor = codigoVendedor;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.cedula = cedula;
    }

    public Integer getIdCliente() {
        return this.idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getCodigoVendedor() {
        return this.codigoVendedor;
    }

    public void setCodigoVendedor(Integer codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getCedula() {
        return this.cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public ClienteModel idCliente(Integer idCliente) {
        setIdCliente(idCliente);
        return this;
    }

    public ClienteModel codigoVendedor(Integer codigoVendedor) {
        setCodigoVendedor(codigoVendedor);
        return this;
    }

    public ClienteModel nombre(String nombre) {
        setNombre(nombre);
        return this;
    }

    public ClienteModel apellido(String apellido) {
        setApellido(apellido);
        return this;
    }

    public ClienteModel direccion(String direccion) {
        setDireccion(direccion);
        return this;
    }

    public ClienteModel cedula(Integer cedula) {
        setCedula(cedula);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ClienteModel)) {
            return false;
        }
        ClienteModel clienteModel = (ClienteModel) o;
        return Objects.equals(idCliente, clienteModel.idCliente) && Objects.equals(codigoVendedor, clienteModel.codigoVendedor) && Objects.equals(nombre, clienteModel.nombre) && Objects.equals(apellido, clienteModel.apellido) && Objects.equals(direccion, clienteModel.direccion) && Objects.equals(cedula, clienteModel.cedula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCliente, codigoVendedor, nombre, apellido, direccion, cedula);
    }

    @Override
    public String toString() {
        return "{" +
            " idCliente='" + getIdCliente() + "'" +
            ", codigoVendedor='" + getCodigoVendedor() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", apellido='" + getApellido() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", cedula='" + getCedula() + "'" +
            "}";
    }
    
} 

