package jedi.yoda.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.yoda.models.VendedorModel;
import jedi.yoda.repository.VendedorRepository;

@Service
public class VendedorService {
    
    @Autowired
    VendedorRepository vendedorRepository;

    public ArrayList<VendedorModel> obtenerVendedor(){
        return (ArrayList<VendedorModel>) vendedorRepository.findAll();
    }

    public VendedorModel guardarVendedor(VendedorModel vendedor){
        return vendedorRepository.save(vendedor);    
    }

    public boolean eliminarVendedor(Integer idVendedor){
        try{
            vendedorRepository.deleteById(idVendedor);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public VendedorModel actualizarCliente(VendedorModel vendedor){
        return vendedorRepository.save(vendedor);    
    }

    public Optional<VendedorModel> obtenerPorId(Integer idVendedor){
        return vendedorRepository.findById(idVendedor);
    }
    

    
}
