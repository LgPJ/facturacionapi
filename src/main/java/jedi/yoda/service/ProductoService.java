package jedi.yoda.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.yoda.models.ProductoModel;
import jedi.yoda.repository.ProductoRepository;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public ArrayList<ProductoModel> obtenerProducto(){
        return (ArrayList<ProductoModel>) productoRepository.findAll();
    }

    public ProductoModel guardarProducto(ProductoModel producto){
        return productoRepository.save(producto);    
    } 

    public boolean eliminarProducto(Long idProducto){
        try{
            productoRepository.deleteById(idProducto);
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
