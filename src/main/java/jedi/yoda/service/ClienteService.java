package jedi.yoda.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.yoda.models.ClienteModel;
import jedi.yoda.repository.ClienteRepository;

@Service
public class ClienteService {
    
    @Autowired
    ClienteRepository clienteRepository;

    public ArrayList<ClienteModel> obtenerCliente(){
        return (ArrayList<ClienteModel>) clienteRepository.findAll();
    }

    public ClienteModel guardarCliente(ClienteModel cliente){
        return clienteRepository.save(cliente);    
    }

    public boolean eliminarCliente(Integer idCliente){
        try{
            clienteRepository.deleteById(idCliente);
            return true;
        } catch (Exception e){
            return false;
        }
    }
    
    public ClienteModel actualizarCliente(ClienteModel cliente){
        return clienteRepository.save(cliente);    
    }
    
    public ArrayList<ClienteModel> obtenerPorCedula(Integer cedula){
        
        ArrayList<ClienteModel> cliente = clienteRepository.findByCedula(cedula);
        
        return cliente;

    }

    public boolean eliminarPorCedula(Integer cedula){
        try{
            clienteRepository.deleteByCedula(cedula);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public Optional<ClienteModel> obtenerPorId(Integer idCliente){
        return clienteRepository.findById(idCliente);
    }

    

}
